
-- SUMMARY --

The Ubercart Product Quote module allows store administrators to specify which
shipping methods/services to enable on a per product and product class basis.

For a full description of the module, visit the project page:
  http://drupal.org/project/uc_product_quote

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/uc_product_quote


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  uc_product_quote module:

  - administer product quotes

    Users in roles with the "administer product quotes" permission will see the
    "Shipping Quotes" tab added to the product edit page.


-- TROUBLESHOOTING --

* If the "Shipping Quotes" tab does not display, check the following:

  - Is the "administer product quotes" permission enabled for the appropriate
    roles?


-- CONTACT --

Current maintainers:
* Jon Antoine (AntoineSolutions) - http://drupal.org/user/192192

This project has been sponsored by:
* Island Creek Oysters
  Committed to growing the finest oysters in the world while having a damn good
  time doing it. Visit http://www.islandcreekoysters.com for more information.

